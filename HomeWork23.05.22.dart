

import 'dart:io';

void main(List<String> args) {
  print('HomeWork: if, else, switch case, host function\n');
  print('1-Определить в какую декаду месяца попадает это число\n2-Определите в какую пору года попадает этот месяц\n3-строка(abcde) проверить первым символом является ли буква (a)\n4-строка(12345) проверить первым символом является 1,2,3\n5-Дана строка из 3-х цифр. Найдите сумму этих цифр\n6-Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется сумме вторых трех цифр');
  String choice = stdin.readLineSync()!;

  switch(choice) {
    case '1':
  print ('Enter the day of the month from 1 to 31');
  int day = int.parse(stdin.readLineSync()!);
  exersice1(day);
  break;

    case '2':
  print('Enter the number of the month from 1 to 12');
  int month = int.parse(stdin.readLineSync()!);
  exersice2(month);
  break;

    case '3':
  print(exersice3('abcde'));
  break;

    case '4':
  print(exersice4('12345'));
  break;

    case '5':
  print('Enter a string of three numbers and try to sum them');
  String num = stdin.readLineSync()!;
  print('sum: ${exersice5(num)}');
  break;

    case '6':
  print('Enter a string of six numbers to sum and to compare two halves for equality');
  String num = stdin.readLineSync()!;
  exersice6(num);
  break;
    default:print('Error');
  }
}


int exersice1(int day){
  dynamic decade =0;
  if (day <= 10 ) {
    decade = 1;
  }
  else if (day >= 11 && day <=20) {
    decade = 2;
  }
  else if (day >= 21 && day <=31) {
    decade = 3;
  }
  else{decade='error';}
  print('Decade: $decade:');
  return day;
}

int exersice2(int month){
  String result ='';
  if (month == 12 || month <= 2) {
    result = 'Winter';
  }
  else if (month >= 3 && month <= 5) {
    result = 'Spring';
  }
  else if (month >= 6 && month <= 8) {
    result = 'Summer';
  }
  else if (month >= 9 && month <= 11) {
    result = 'Autumn';
  }
  print(result);
  return month;
}

String exersice3(String words) {
  switch(words[0]){
    case 'a':
      return ('Yes, letter (a) is the first symbol in this string');
    default:
      return('Not, letter (a) are not the first symbol of this string');
  }
  }

  String exersice4(String numbers){
  switch(numbers[0]){
    case'1':
      return 'Yes, number (1) is the first symbol in this string';
    case '2':
      return 'Yes, number (2) is the first symbol in this string';
    case '3':
      return 'Yes, number (3) is the first symbol in this string';
    default:
      return 'Not, the numbers (1,2,3) are not the first symbol of this string';
  }
  }

int exersice5(String number){
  return int.parse (number[0]) + int.parse (number[1]) + int.parse(number[2]);
}


String exersice6(String number){
int sum1 = int.parse (number[0]) + int.parse (number[1]) + int.parse(number[2]);
int sum2 = int.parse (number[3]) + int.parse (number[4]) + int.parse(number[5]);
if(sum1==sum2){
  print('Yes, First half: $sum1 equal Second half: $sum2');
}
else{print('Not, The first half: $sum1 is not equal to the second half: $sum2');
}
return number;
}
